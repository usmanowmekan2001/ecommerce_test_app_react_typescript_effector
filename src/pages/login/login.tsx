import { Link } from 'react-router-dom';
import styles from './login.module.css';
import {
  $email,
  $emailError,
  $error,
  $formDisabled,
  $password,
  $passwordError,
  emailChanged,
  formSubmitted,
  passwordChanged,
} from './model/model';
import { Spinner } from '../../shared/lib/spinner/spinner';
import { useUnit } from 'effector-react';
import { setSpinner } from '../../shared/lib/spinner/model';
import { BlockLogin } from '../../features/check-auth';

export interface LoginProps {}
export function Login(props: LoginProps) {
  const [email, password, formDisabled, emailError, passwordError] = useUnit([
    $email,
    $password,
    $formDisabled,
    $emailError,
    $passwordError,
  ]);
  const handleAuth = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setSpinner(true);
    formSubmitted();
  };
  return (
    <BlockLogin>
      <div className={styles['container']}>
        <div className={styles['authCard']}>
          <h3>Login</h3>
          <form action="" onSubmit={handleAuth} className={styles['authForm']}>
            <input
              className="form-control"
              type="email"
              placeholder="Email"
              name="email"
              value={email}
              onChange={(e) => emailChanged(e.target.value)}
              disabled={formDisabled}
            />
            {emailError ? (
              <p style={{ color: 'red' }}>{emailErrorText[emailError]}</p>
            ) : (
              ''
            )}
            <input
              className="form-control"
              type="password"
              placeholder="Password"
              name="password"
              value={password}
              onChange={(e) => passwordChanged(e.target.value)}
              disabled={formDisabled}
            />
            {passwordError ? (
              <span style={{ color: 'red' }}>
                {passwordErrorText[passwordError]}
              </span>
            ) : (
              ''
            )}
            <ErrorView />
            <div>
              <Link to="/forgot-password">Forgot Password?</Link>
              <div className={styles['buttons']}>
                {formDisabled ? (
                  <Spinner top={5} left={20} />
                ) : (
                  <button
                    className={styles['button']}
                    disabled={formDisabled}
                    type="submit"
                  >
                    Login
                  </button>
                )}
              </div>
            </div>
          </form>
        </div>
      </div>
    </BlockLogin>
  );
}
const passwordErrorText = {
  empty: 'Пароль не может быть пустым',
  invalid: 'Пароль слишком короткий',
};
const emailErrorText = {
  empty: 'Email не может быть пустым',
  invalid: 'Неверный формат e-mail',
};

function ErrorView() {
  const error = useUnit($error);

  if (!error) {
    return <p />;
  }

  if (error?.error === 'Invalid Credentials') {
    return <p style={{ color: 'red' }}>Неверный пароль и/или почта</p>;
  }

  return (
    <p style={{ color: 'red' }}>
      Что-то пошло не так, попробуйте еще раз, пожалуйста
    </p>
  );
}
export default Login;
