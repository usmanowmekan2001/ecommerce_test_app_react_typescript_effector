import {
  attach,
  createEffect,
  createEvent,
  createStore,
  sample,
} from 'effector';
import { handleAlertMessage } from '../../../shared/lib/alert/model/model';
import { and, every, not, reset } from 'patronum';
import * as api from '../../../shared/api/auth';
import { IUser } from '../types/types';
import { $router } from '../../../shared/router';
const loginFx = attach({ effect: api.loginFx });
type SignInError =
  | { error: 'Invalid Credentials' }
  | { error: 'invalid_request' };

export interface IUserData {
  firstname: string;
  lastname: string;
  email: string;
  mobile: number;
  password: string;
  token: string;
  _id: string;
}

export const setAuth = createEvent<boolean>();

export const emailChanged = createEvent<string>();
export const passwordChanged = createEvent<string>();
export const formSubmitted = createEvent();

export const $email = createStore('').on(emailChanged, (_, value) => value);
export const $emailError = createStore<null | 'empty' | 'invalid'>(null);
export const $formDisabled = loginFx.pending;

export const $password = createStore('').on(
  passwordChanged,
  (_, value) => value
);
export const $passwordError = createStore<null | 'empty' | 'invalid'>(null);

// export const $formDisabled = createStore<boolean>(false);
export const $error = createStore<SignInError | null>(null);
const $formValid = every({
  stores: [$emailError, $passwordError],
  predicate: null,
});
$error.reset(formSubmitted);

export const $auth = createStore<boolean>(false).on(
  setAuth,
  (_, value) => value
);

sample({
  clock: formSubmitted,
  source: $email,
  fn: (email) => {
    if (isEmpty(email)) return 'empty';
    if (!isEmailValid(email)) return 'invalid';
    return null;
  },
  target: $emailError,
});
sample({
  clock: formSubmitted,
  source: $password,
  fn: (password) => {
    if (isEmpty(password)) return 'empty';
    if (!isPasswordValid(password)) return 'invalid';
    return null;
  },
  target: $passwordError,
});
const login = createEffect(async (form: IUser) => {
  const userData = await loginFx(form);
  localStorage.setItem('user', JSON.stringify(userData));
  localStorage.setItem('token', JSON.stringify(userData.token));
});
sample({
  clock: formSubmitted,
  source: { email: $email, password: $password },
  filter: and(not($formDisabled), $formValid),
  target: login,
});

sample({
  clock: loginFx.doneData,
  target: createEffect(() => {
    setAuth(true);
  }),
});
reset({
  clock: loginFx.doneData,
  target: [$email, $password],
});

const routeFx = attach({
  source: $router,
  effect: (router) => {
    if (!router) throw new Error('ROuter is not available');
    router.navigate('/admin');
  },
});

sample({
  clock: loginFx.doneData,
  target: routeFx,
});
$error.on(loginFx.failData, (_, error) => error);

export const handleError = () => {
  handleAlertMessage({
    alertText: 'Something went wrong',
    alertStatus: 'warning',
  });
};

function isEmailValid(email: string) {
  return email.includes('@') && email.length > 5;
}

function isPasswordValid(password: string) {
  return password.length > 5;
}

function isEmpty(input: string) {
  return input.trim().length === 0;
}
