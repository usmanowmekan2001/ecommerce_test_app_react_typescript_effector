export interface ISpinnerProps {
  top: number;
  left: number;
}
export interface IAlert {
  alertText: string;
  alertStatus: string;
}
export interface IUser {
  email: string;
  password: string;
}
export interface ILoginData {
  email: string;
  password: string;
}