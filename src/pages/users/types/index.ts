export interface IUnSortedData {
  key: number;
  name: string;
  email: string;
  mobile: string;
}
export interface IUsersItem {
  address: string;
  cart: object[];
  createdAt: string;
  updatedAt: string;
  email: string;
  firstname: string;
  isBlocked: boolean;
  lastname: string;
  mobile: string;
  password: string;
  refreshToken: string;
  role: string;
  wishlist: string[];
  __v: number;
  _id: string;
}