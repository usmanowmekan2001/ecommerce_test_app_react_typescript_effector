import { useEffect, useState } from 'react';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import styles from './users.module.css';
import { $users, $usersLoading, getAllUsers } from './model';
import { useUnit } from 'effector-react';
import CheckAuth from '../../features/check-auth';
/* eslint-disable-next-line */
export interface UsersProps {}

export function Users(props: UsersProps) {
  useEffect(() => {
    getAllUsers();
  }, []);
  const [customerState, usersLoading] = useUnit([$users, $usersLoading]);
  const [activeNumber, setActiveNumber] = useState(1);
  const pageNumbersData: string[] = [];

  const countPageNumbers = Math.ceil(customerState.length / 3);
  for (let i = 1; i <= countPageNumbers; i++) {
    pageNumbersData.push(String(i));
  }
  console.log(usersLoading);

  return (
    <CheckAuth>
      <div className={styles['container']}>
        <div className={styles['recentRow']}>
          <div className={styles['recentRowTable']}>
            <h3 className={styles['title']}>Customers</h3>
            {!usersLoading ? (
              <table>
                <thead>
                  <tr>
                    <th className={styles['tableTitle']}>No.</th>
                    <th className={styles['sortTableTitle']}>
                      <span>Name</span>
                    </th>
                    <th className={styles['tableTitle']}>Email</th>
                    <th className={styles['tableTitle']}>Mobile</th>
                  </tr>
                </thead>
                <tbody>
                  {customerState
                    .slice((activeNumber - 1) * 3, (activeNumber - 1) * 3 + 3)
                    .map((el, index) => {
                      return (
                        <tr key={index}>
                          <td>{el.key}</td>
                          <td>{el.name}</td>
                          <td>{el.email}</td>
                          <td>{el.mobile}</td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
            ) : (
              <p>Loading...</p>
            )}
          </div>
        </div>
        <div className={styles['page']}>
          <div
            className={styles['pageIcon']}
            onClick={() =>
              setActiveNumber((prev) => (prev !== 1 ? prev - 1 : 1))
            }
          >
            <IoIosArrowBack />
          </div>
          <div className={styles['pageNumbers']}>
            {pageNumbersData.map((number, index) => {
              return (
                <span
                  id={String(index + 1)}
                  key={index}
                  className={
                    activeNumber === Number(number)
                      ? `${styles['pageNumber']} ${styles['activeNumber']}`
                      : styles['pageNumber']
                  }
                  onClick={(e) => {
                    setActiveNumber(Number(e.currentTarget.id));
                    console.log(e.currentTarget.id);
                  }}
                >
                  {number}
                </span>
              );
            })}
          </div>
          <div
            className={styles['pageIcon']}
            onClick={() =>
              setActiveNumber((prev) =>
                prev === countPageNumbers ? countPageNumbers : prev + 1
              )
            }
          >
            <IoIosArrowForward />
          </div>
        </div>
      </div>
    </CheckAuth>
  );
}

export default Users;
