import { attach, createEvent, createStore } from 'effector';
import { IUnSortedData, IUsersItem } from '../types';
import * as api from '../../../shared/api/auth';

const getUsersFx = attach({ effect: api.getUsersFx });
export const setUsers = createEvent<IUnSortedData[]>();
const fetchUsers = createEvent<boolean>();
export const $usersLoading = createStore<boolean>(true).on(
  fetchUsers,
  (_, value) => value
);
export const $users = createStore<IUnSortedData[]>([]).on(
  setUsers,
  (_, value) => value
);

export const getAllUsers = async () => {
  fetchUsers(true);
  const allUsers: IUsersItem[] = await getUsersFx();
  const tableData: IUnSortedData[] = [];
  for (let i = 0; i < allUsers.length; i++) {
    if (allUsers[i].role !== 'admin') {
      tableData.push({
        key: i + 1,
        name: allUsers[i].firstname + ' ' + allUsers[i].lastname,
        email: allUsers[i].email,
        mobile: allUsers[i].mobile,
      });
    }
  }
  setUsers(tableData);
  fetchUsers(false);
};
