import {
  attach,
  createEffect,
  createEvent,
  createStore,
  sample,
} from 'effector';
import * as api from '../../../shared/api/auth';

const getOrdersFx = attach({ effect: api.getOrdersFx });
const getMonthlyOrderFx = attach({ effect: api.getMonthlyOrderFx });
const getYearlyStatsFx = attach({ effect: api.getYearlyStatsFx });

interface IMonthlyOrders {
  amount: number;
  count: number;
  _id: { month: string };
}
interface IProductItem {
  color: string;
  price: number;
  product: string;
  quantity: number;
  _id: string;
}
interface IShippingInfo {
  address: string;
  city: string;
  firstName: string;
  lastName: string;
  other: string;
  pinCode: number;
  state: string;
}
interface IUsersItem {
  address: string;
  cart: object[];
  createdAt: string;
  updatedAt: string;
  email: string;
  firstname: string;
  isBlocked: boolean;
  lastname: string;
  mobile: string;
  password: string;
  refreshToken: string;
  role: string;
  wishlist: string[];
  __v: number;
  _id: string;
}
interface IOrder {
  createdAt: string;
  month: string;
  orderItems: IProductItem[];
  orderStatus: string;
  paidAt: string;
  paymentInfo: { razorpayOrderId: string };
  shippingInfo: IShippingInfo;
  totalPrice: number;
  totalPriceAfterDiscount: number;
  updatedAt: string;
  user: IUsersItem;
  __v: number;
  _id: string;
}
interface IMonthlyData {
  month: string;
  income: number;
}
interface IYearlyData {
  amount: number;
  count: number;
  _id: string;
}
interface IMonthlyDataSales {
  month: string;
  value: number;
}
interface IOrderData {
  key: number;
  name: string;
  product: number;
  price: number;
  afterPrice: number;
  status: string;
}
export const dashBoardPageMounted = createEvent();
const monthlyData = createEvent<IMonthlyData[]>();
const monthlySales = createEvent<IMonthlyDataSales[]>();
const yearlyOrders = createEvent<IYearlyData[]>();
const allOrders = createEvent<IOrderData[]>();
export const $monthlyData = createStore<IMonthlyData[]>([]).on(
  monthlyData,
  (_, value) => value
);
export const $monthlySales = createStore<IMonthlyDataSales[]>([]).on(
  monthlySales,
  (_, value) => value
);
export const $yearlyOrders = createStore<IYearlyData[]>([]).on(
  yearlyOrders,
  (_, value) => value
);
export const $orders = createStore<IOrderData[]>([]).on(
  allOrders,
  (_, value) => value
);
const initialDates = createEffect(async () => {
  const monthlyDates: IMonthlyOrders[] = await getMonthlyOrderFx();
  const yearlyDates: IYearlyData[] = await getYearlyStatsFx();
  const getOrders: IOrder[] = await getOrdersFx();
  const data1: IOrderData[] = [];

  for (let i = 0; i < getOrders?.length; i++) {
    data1.push({
      key: i + 1,
      name: getOrders[i]?.user?.firstname + ' ' + getOrders[i]?.user?.lastname,
      product: getOrders[i]?.orderItems?.length,
      price: getOrders[i]?.totalPrice,
      afterPrice: getOrders[i]?.totalPriceAfterDiscount,
      status: getOrders[i]?.orderStatus,
    });
  }
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  const monthlyOrderCount: IMonthlyDataSales[] = [];
  const data: IMonthlyData[] = [];
  for (let i = 0; i < monthlyDates?.length; i++) {
    const element = monthlyDates[i];

    data.push({
      month: monthNames[Number(element?._id?.month)],
      income: element?.amount,
    });
    monthlyOrderCount.push({
      month: monthNames[Number(element?._id?.month)],
      value: element?.count,
    });
  }
  monthlyData(data);
  monthlySales(monthlyOrderCount);
  yearlyOrders(yearlyDates);
  allOrders(data1);
});
sample({
  clock: dashBoardPageMounted,
  target: initialDates,
});
