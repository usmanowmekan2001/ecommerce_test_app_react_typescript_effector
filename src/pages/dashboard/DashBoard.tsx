import React, { useEffect, useState } from 'react';
import styles from './Dashboard.module.css';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import { Bar } from 'react-chartjs-2';
import { Chart, registerables } from 'chart.js';
import {
  $monthlyData,
  $monthlySales,
  $orders,
  $yearlyOrders,
  dashBoardPageMounted,
} from './model/model';
import { useUnit } from 'effector-react';
const Dashboard = () => {
  const [monthlyData, monthlyDataSales, yearlyOrders, orders] = useUnit([
    $monthlyData,
    $monthlySales,
    $yearlyOrders,
    $orders,
  ]);

  console.log('begin');
  useEffect(() => {
    dashBoardPageMounted();
  }, []);

  const [activeNumber, setActiveNumber] = useState(1);
  const pageNumbersData = [];
  const countPageNumbers = Math.ceil(orders.length / 5);
  for (let i = 1; i <= countPageNumbers; i++) {
    pageNumbersData.push(String(i));
  }
  Chart.register(...registerables);
  const chartData = {
    labels: monthlyData.map((item) => item.month),
    datasets: [
      {
        label: '',
        data: monthlyData.map((item) => item.income),
        backgroundColor: ['#ffd333'],
        borderColor: '#000',
        fill: '#FFFFFF',
        hoverBackgroundColor: '#ccc',
        // borderWidth: 2,
      },
    ],
  };
  const chartDataSales = {
    labels: monthlyDataSales.map((item) => item.month),
    datasets: [
      {
        label: '',
        data: monthlyDataSales.map((item) => item.value),
        backgroundColor: ['#ffd333'],
        borderColor: '#000',
        fill: '#FFFFFF',
        hoverBackgroundColor: '#ccc',
        // borderWidth: 2,
      },
    ],
  };
  const handleBack = () => {
    setActiveNumber((prev) => (prev !== 1 ? prev - 1 : 1));
  };
  const handleNext = () => {
    setActiveNumber((prev) =>
      prev === countPageNumbers ? countPageNumbers : prev + 1
    );
  };
  return (
    <div className={styles.dashboardWrapper}>
      <h3 className={styles.dashboardTitle}>Dashboard</h3>
      <div className={styles.dashboardCards}>
        <div className={styles.dashboardCart}>
          <div className={styles.cartLeftElements}>
            <p className={styles.desc}>Total Income</p>
            <h4 className={styles.subTitle}>
              $ {yearlyOrders && yearlyOrders[0]?.amount}
            </h4>
          </div>
          <div className={styles.cardRightElements}>
            <p className={styles.desc}>Income in Last Year from Today</p>
          </div>
        </div>
        <div className={styles.dashboardCart}>
          <div className={styles.cartLeftElements}>
            <p className={styles.desc}>Total Sales</p>
            <h4 className={styles.subTitle}>
              {yearlyOrders && yearlyOrders[0]?.count}
            </h4>
          </div>
          <div className={styles.cardRightElements}>
            <p className={styles.desc}>Sales in Last Year from Today</p>
          </div>
        </div>
      </div>
      <div className={styles.charts}>
        <div className={styles.dashboardBarChart}>
          <h3 className={styles.title}>Income Statics</h3>
          <div className={styles.dashboardBar}>
            <Bar
              data={chartData}
              options={{
                plugins: {
                  title: {
                    display: false,
                  },
                  legend: {
                    display: false,
                  },
                },
                scales: {
                  y: {
                    beginAtZero: true,
                  },
                },
              }}
            />
          </div>
        </div>
        <div className={styles.dashboardBarChart}>
          <h3 className={styles.title}>Sales Statics</h3>
          <div className={styles.dashboardBar}>
            <Bar
              data={chartDataSales}
              options={{
                plugins: {
                  title: {
                    display: false,
                  },
                  legend: {
                    display: false,
                  },
                },
                scales: {
                  y: {
                    beginAtZero: true,
                  },
                },
              }}
            />
          </div>
        </div>
      </div>
      <div className={styles.recentRow}>
        <h3 className={styles.title}>Recent Orders</h3>
        <div className={styles.recentRowTable}>
          <table>
            <thead>
              <tr>
                <th className={styles.tableTitle}>No.</th>
                <th className={styles.tableTitle}>Name</th>
                <th className={styles.tableTitle}>Product Count</th>
                <th className={styles.tableTitle}>Total Price</th>
                <th className={styles.tableTitle}>
                  Total Price After Discount
                </th>
                <th className={styles.tableTitle}>Status</th>
              </tr>
            </thead>
            <tbody>
              {orders
                .slice((activeNumber - 1) * 5, (activeNumber - 1) * 5 + 5)
                .map((el, index) => {
                  return (
                    <tr key={index}>
                      <td>{el.key}</td>
                      <td>{el.name}</td>
                      <td>{el.product}</td>
                      <td>{el.price}</td>
                      <td>{el.afterPrice}</td>
                      <td>{el.status}</td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
          <div className={styles.page}>
            <div className={styles.pageIcon} onClick={handleBack}>
              <IoIosArrowBack />
            </div>
            <div className={styles.pageNumbers}>
              {pageNumbersData.map((number, index) => {
                return (
                  <span
                    id={String(index + 1)}
                    key={index}
                    className={
                      activeNumber === Number(number)
                        ? `${styles.pageNumber} ${styles.activeNumber}`
                        : styles.pageNumber
                    }
                    onClick={(e) => {
                      setActiveNumber(Number(e.currentTarget.id));
                    }}
                  >
                    {number}
                  </span>
                );
              })}
            </div>
            <div className={styles.pageIcon} onClick={handleNext}>
              <IoIosArrowForward />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
