import { useLocation, useNavigate } from 'react-router-dom';
import styles from './delete-product.module.css';
import { deletingId } from './model';

/* eslint-disable-next-line */
export interface DeleteProductProps {}

export function DeleteProduct(props: DeleteProductProps) {
  const navigate = useNavigate();
  const location = useLocation();
  const deleteProduct = async () => {
    deletingId(location.pathname.split('/')[3]);
  };
  return (
    <div className={styles['editProduct']}>
      <div className={styles['deleteBody']}>
        <div className={styles['editTitle']}>
          Do You want to delete this product
        </div>
        <div className={styles['buttons']}>
          <button
            onClick={() => navigate('/admin/product-list')}
            className={styles['cancelButton']}
          >
            Cancel
          </button>
          <button className={styles['deleteButton']} onClick={deleteProduct}>
            Delete
          </button>
        </div>
      </div>
    </div>
  );
}

export default DeleteProduct;
