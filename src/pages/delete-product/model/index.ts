import { IAlert } from './../../login/types/types';
import { setAlert } from '../../../shared/lib/alert/model/model';
import { attach, createEvent, createStore, sample } from 'effector';
import { $router } from '../../../shared/router';
import * as api from '../../../shared/api/product';

const deleteProductFx = attach({ effect: api.deleteProductFx });

export const deletingId = createEvent<string>();
export const $deletingId = createStore<string>('').on(
  deletingId,
  (_, value) => value
);
export const handleAlertMessage = (alert: IAlert) => {
  setAlert(alert);
  setTimeout(() => setAlert({ alertText: '', alertStatus: '' }), 3000);
};
sample({
  clock: deletingId,
  source: $deletingId,
  target: deleteProductFx,
});

const routeFx = attach({
  source: $router,
  effect: (router) => {
    if (!router) throw new Error('ROuter is not available');
    router.navigate('/admin/product-list');
  },
});

sample({
  clock: deleteProductFx.doneData,
  target: routeFx,
});
