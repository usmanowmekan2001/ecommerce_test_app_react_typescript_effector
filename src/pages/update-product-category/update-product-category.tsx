import { useLocation, useNavigate } from 'react-router-dom';
import { Spinner } from '../../shared/lib/spinner/spinner';
import styles from './update-product-category.module.css';
import { useEffect } from 'react';
import {
  $updateFormDisabled,
  $updatingProductCategory,
  $updatingProductCategoryError,
  setUpdatingCategoryId,
  setUpdatingProductCategory,
  updateFormSubmitted,
  updatePageMounted,
} from './model';
import { useUnit } from 'effector-react';
import { $spinner, setSpinner } from '../../shared/lib/spinner/model';

/* eslint-disable-next-line */
export interface UpdateProductCategoryProps {}

export function UpdateProductCategory(props: UpdateProductCategoryProps) {
  const navigate = useNavigate();
  const location = useLocation();
  useEffect(() => {
    updatePageMounted();
    setUpdatingCategoryId(location.pathname.split('/')[3]);
  }, []);

  const [
    updatingProductCategory,
    updatingProductCategoryError,
    spinner,
    updateFormDisabled,
  ] = useUnit([
    $updatingProductCategory,
    $updatingProductCategoryError,
    $spinner,
    $updateFormDisabled,
  ]);
  function handleUpdate(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    setSpinner(true);
    updateFormSubmitted();
  }
  return (
    <div className={styles['editProduct']}>
      <div className={styles['editBody']}>
        <div className={styles['editTitle']}>Update Category</div>
        <form action="" onSubmit={handleUpdate} className={styles['authForm']}>
          <input
            className="form-control py-3 mb-3"
            type="text"
            placeholder="title"
            name="title"
            value={updatingProductCategory}
            onChange={(e) => setUpdatingProductCategory(e.target.value)}
            onBlur={(e) => setUpdatingProductCategory(e.target.value)}
            disabled={updateFormDisabled}
          />
          {updatingProductCategoryError ? (
            <span style={{ color: 'red' }}>{updateErrorText.empty}</span>
          ) : (
            ''
          )}
          <div>
            <div className={styles['buttons']}>
              <button
                disabled={updateFormDisabled}
                className={styles['cancelButton']}
                onClick={() => {
                  navigate('/admin/category-list');
                  setSpinner(false);
                }}
              >
                Cancel
              </button>
              {spinner ? (
                <Spinner top={5} left={20} />
              ) : (
                <button
                  type="submit"
                  disabled={updateFormDisabled}
                  className={styles['deleteButton']}
                >
                  Update Product
                </button>
              )}
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

const updateErrorText = {
  empty: 'Поля не может быть пустым',
};
export default UpdateProductCategory;
