import { $router } from '../../../shared/router';
import {
  attach,
  createEffect,
  createEvent,
  createStore,
  sample,
} from 'effector';
import * as api from '../../../shared/api/category';
import { not, reset } from 'patronum';
// const defaultUpdatingState = {
//   createdAt: '',
//   updatedAt: '',
//   title: '',
//   _id: '',
//   __v: 0,
// };
const getProductCategoryFx = attach({ effect: api.getProductCategoryFx });
const updateProductCategoryFx = attach({ effect: api.updateProductCategoryFx });
export const updatePageMounted = createEvent();
export const updateFormSubmitted = createEvent();
export const setUpdatingProductCategory = createEvent<string>();
export const setUpdatingCategoryId = createEvent<string>();
const $updatingCategoryId = createStore<string>('').on(
  setUpdatingCategoryId,
  (_, value) => value
);
const setLocation = createEvent<string>();
export const $location = createStore<string | null>(null).on(
  setLocation,
  (_, value) => value
);
export const $updatingProductCategory = createStore<string>('').on(
  setUpdatingProductCategory,
  (_, value) => value
);
export const $updatingProductCategoryError = createStore<boolean>(false);
export const $updateFormDisabled = updateProductCategoryFx.pending;
$updatingProductCategoryError.reset(updateFormSubmitted);

const getCategoryTitle = createEffect(async (id: string) => {
  const title = await getProductCategoryFx(id);
  setUpdatingProductCategory(title);
});
reset({
  clock: updatePageMounted,
  target: [$updatingProductCategory, $updatingProductCategoryError],
});
sample({
  clock: updatePageMounted,
  source: $updatingCategoryId,
  target: getCategoryTitle,
});

sample({
  clock: updateFormSubmitted,
  source: $updatingProductCategory,
  fn: (updatingProductCategory) => {
    if (isEmpty(updatingProductCategory)) return true;
    return false;
  },
  target: $updatingProductCategoryError,
});
sample({
  clock: updateFormSubmitted,
  source: { id: $updatingCategoryId, title: $updatingProductCategory },
  filter: not($updateFormDisabled),
  target: updateProductCategoryFx,
});
const routeFx = attach({
  source: $router,
  effect: (router) => {
    if (!router) throw new Error('ROuter is not available');
    router.navigate('/admin/category-list');
  },
});
sample({
  clock: updateProductCategoryFx.doneData,
  target: routeFx,
});

function isEmpty(input: string) {
  return input.trim().length === 0;
}
