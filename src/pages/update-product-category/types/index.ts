export interface IAlert {
  alertText: string;
  alertStatus: string;
}
export interface IProductCategoryData {
  id: string;
  title: string;
}
export interface ICategoryItem {
  createdAt: string;
  title: string;
  updatedAt: string;
  __v: number;
  _id: string;
}