import { $router } from '../../../shared/router';
import * as api from '../../../shared/api/product';
import * as brands from '../../../shared/api/brand';
import * as category from '../../../shared/api/category';
import * as color from '../../../shared/api/color';
// import * as router from '../../../shared/routing';
import { IBrandItem, ICategoryItem, IColorItem } from '../types';
import {
  attach,
  createEffect,
  createEvent,
  createStore,
  sample,
} from 'effector';
import { not } from 'patronum';

const updateProductFx = attach({ effect: api.updateProductFx });
const getBrandsFx = attach({ effect: brands.getBrandsFx });
const getColorsFx = attach({ effect: color.getColorsFx });
const getCategoriesFx = attach({ effect: category.getCategoriesFx });
const getSingleProductFx = attach({ effect: api.getSingleProductFx });

export const setColors = createEvent<IColorItem[]>();
export const setCategories = createEvent<ICategoryItem[]>();
export const setBrands = createEvent<IBrandItem[]>();
export const updateProductFormSubmitted = createEvent();

export const setBrand = createEvent<string>();
export const setTitle = createEvent<string>();
export const setPrice = createEvent<number>();
export const setQuantity = createEvent<number>();
export const setCategory = createEvent<string>();
export const setColor = createEvent<string[]>();
export const pageMounted = createEvent<string>();

export const $updateProductFormDisabled = updateProductFx.pending;
export const $brand = createStore<string>('').on(setBrand, (_, value) => value);
export const $title = createStore<string>('').on(setTitle, (_, value) => value);
export const $price = createStore<number>(0).on(setPrice, (_, value) => value);
export const $quantity = createStore<number>(0).on(
  setQuantity,
  (_, value) => value
);
export const $category = createStore<string>('').on(
  setCategory,
  (_, value) => value
);
export const $color = createStore<string[]>([]).on(
  setColor,
  (_, value) => value
);
export const $updatingProductId = createStore<string>('').on(
  pageMounted,
  (_, value) => value
);
export const $colors = createStore<IBrandItem[]>([]).on(
  setColors,
  (_, value) => value
);
export const $categories = createStore<ICategoryItem[]>([]).on(
  setCategories,
  (_, value) => value
);
export const $brands = createStore<IBrandItem[]>([]).on(
  setBrands,
  (_, value) => value
);

const getProduct = createEffect((id: string) => {
  console.log(id);
  getSingleProductFx(id).then((product) => {
    setBrand(product.brand);
    setTitle(product.title);
    setPrice(product.price);
    setQuantity(product.quantity);
    setCategory(product.category);
    setColor(product.color);
  });
});
const getInitialDates = createEffect(() => {
  getBrandsFx().then((brands) => setBrands(brands));
  getColorsFx().then((colors) => setColors(colors));
  getCategoriesFx().then((categories) => setCategories(categories));
});
sample({
  clock: pageMounted,
  source: $updatingProductId,
  target: getProduct,
});
sample({
  clock: pageMounted,
  target: getInitialDates,
});

sample({
  clock: updateProductFormSubmitted,
  source: {
    id: $updatingProductId,
    title: $title,
    brand: $brand,
    price: $price,
    category: $category,
    quantity: $quantity,
    color: $color,
  },
  filter: not($updateProductFormDisabled),
  target: updateProductFx,
});
const routeFx = attach({
  source: $router,
  effect: (router) => {
    if (!router) throw new Error('ROuter is not available');
    router.navigate('/admin/product-list');
  },
});
sample({
  clock: updateProductFx.doneData,
  target: routeFx,
});
// const setBeginnerData = createEffect(()=>{
//   set
// })
