export interface IColorOpt {
  label: string;
  value: string;
}
export interface IProductData {
  id: string;
  title: string;
  price: number;
  quantity: number;
  brand: string;
  category: string;
  color: string[];
}
export interface IAlert {
  alertText: string;
  alertStatus: string;
}

export interface IProductItem {
  brand: string;
  category: string;
  color: string[];
  createdAt: string;
  updatedAt: string;
  description: string;
  images: object[];
  ratings: object[];
  price: number;
  quantity: number;
  sold: number;
  slug: string;
  tags: string;
  title: string;
  totalrating: string;
  _id: string;
}
export interface IBrandItem {
  createdAt: string;
  updatedAt: string;
  title: string;
  _id: string;
  __v: number;
}
export interface ICategoryItem extends IBrandItem {}
export interface IColorItem extends IBrandItem {}
