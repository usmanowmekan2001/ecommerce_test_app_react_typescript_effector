import { useLocation, useNavigate } from 'react-router-dom';
import { $spinner, setSpinner } from '../../shared/lib/spinner/model';
import { Spinner } from '../../shared/lib/spinner/spinner';
import styles from './update-product.module.css';
import { useGate, useUnit } from 'effector-react';
import { useEffect } from 'react';
import { Select } from 'antd';

import { IColorOpt } from './types';
import {
  $brand,
  $brands,
  $categories,
  $category,
  $color,
  $colors,
  $price,
  $quantity,
  $title,
  $updateProductFormDisabled,
  pageMounted,
  setBrand,
  setCategory,
  setColor,
  setPrice,
  setQuantity,
  setTitle,
  updateProductFormSubmitted,
} from './model';
import { ProductUpdatePageGate } from '../../shared/router';

/* eslint-disable-next-line */
export interface UpdateProductProps {}

export function UpdateProduct(props: UpdateProductProps) {
  useGate(ProductUpdatePageGate);
  const location = useLocation();
  useEffect(() => {
    pageMounted(location.pathname.split('/')[3]);
  }, []);
  const navigate = useNavigate();

  const [
    brands,
    categories,
    colors,
    spinner,
    brand,
    title,
    price,
    quantity,
    category,
    color,
    updateProductFormDisabled,
  ] = useUnit([
    $brands,
    $categories,
    $colors,
    $spinner,
    $brand,
    $title,
    $price,
    $quantity,
    $category,
    $color,
    $updateProductFormDisabled,
  ]);
  const colorOpt: IColorOpt[] = [];
  colors.forEach((i) => {
    colorOpt.push({
      label: i.title,
      value: i._id,
    });
  });
   function handleUpdate(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    setSpinner(true);
    updateProductFormSubmitted();
  }
  return (
    <div className={styles['editProduct']}>
      <div className={styles['editBody']}>
        <div className={styles['editTitle']}>Update {title} product</div>
        <form action="" onSubmit={handleUpdate} className={styles['authForm']}>
          <input
            className="form-control py-3 mb-3"
            type="text"
            placeholder="title"
            name="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            onBlur={(e) => setTitle(e.target.value)}
            disabled={updateProductFormDisabled}
          />
          <input
            className="form-control py-3 mb-3"
            type="number"
            placeholder="price"
            name="price"
            value={price}
            onChange={(e) => setPrice(Number(e.target.value))}
            onBlur={(e) => setPrice(Number(e.target.value))}
            disabled={updateProductFormDisabled}
          />
          <select
            name="brand"
            value={brand}
            onChange={(e) => setBrand(e.target.value)}
            onBlur={(e) => setBrand(e.target.value)}
            className="form-control py-3 mb-3"
            id=""
            disabled={updateProductFormDisabled}
          >
            <option value="">Select Brand</option>
            {brands.map((i, j) => {
              return (
                <option key={j} value={i.title}>
                  {i.title}
                </option>
              );
            })}
          </select>
          <select
            name="category"
            value={category}
            onChange={(e) => setCategory(e.target.value)}
            onBlur={(e) => setCategory(e.target.value)}
            className="form-control py-3 mb-3"
            id=""
            disabled={updateProductFormDisabled}
          >
            <option value="">Select Category</option>
            {categories.map((i, j) => {
              return (
                <option key={j} value={i.title}>
                  {i.title}
                </option>
              );
            })}
          </select>
          <Select
            mode="multiple"
            allowClear
            className="w-100 mb-3"
            placeholder="Select colors"
            defaultValue={color}
            onChange={(i) => setColor(i)}
            options={colorOpt}
            disabled={updateProductFormDisabled}
          />
          <input
            className="form-control py-3 mb-3"
            type="number"
            placeholder="Quantity"
            name="quantity"
            value={quantity}
            onChange={(e) => setQuantity(Number(e.target.value))}
            onBlur={(e) => setQuantity(Number(e.target.value))}
            disabled={updateProductFormDisabled}
          />

          <div>
            <div className={styles['buttons']}>
              <button
                className={styles['cancelButton']}
                disabled={updateProductFormDisabled}
                onClick={() => {
                  navigate('/admin/product-list');
                  setSpinner(false);
                }}
              >
                Cancel
              </button>
              {spinner ? (
                <Spinner top={5} left={20} />
              ) : (
                <button
                  type="submit"
                  disabled={updateProductFormDisabled}
                  className={styles['deleteButton']}
                >
                  Update Product
                </button>
              )}
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default UpdateProduct;
