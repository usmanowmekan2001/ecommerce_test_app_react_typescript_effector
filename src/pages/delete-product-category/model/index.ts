import { attach, createEvent, createStore, sample } from 'effector';
import { $router } from '../../../shared/router';
import * as api from '../../../shared/api/category';

const deleteProductCategoryFx = attach({ effect: api.deleteProductCategoryFx });
export const setDeletingCategoryId = createEvent<string>();
export const $deletingCategoryId = createStore<string>('').on(
  setDeletingCategoryId,
  (_, value) => value
);

sample({
  clock: setDeletingCategoryId,
  source: $deletingCategoryId,
  target: deleteProductCategoryFx,
});

const routeFx = attach({
  source: $router,
  effect: (router) => {
    if (!router) throw new Error('ROuter is not available');
    router.navigate('/admin/category-list');
  },
});

sample({
  clock: deleteProductCategoryFx.doneData,
  target: routeFx,
});
