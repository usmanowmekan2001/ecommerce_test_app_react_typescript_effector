import { useLocation, useNavigate } from 'react-router-dom';
import styles from './delete-product-category.module.css';
import { setDeletingCategoryId } from './model';

/* eslint-disable-next-line */
export interface DeleteProductCategoryProps {}

export function DeleteProductCategory(props: DeleteProductCategoryProps) {
  const navigate = useNavigate();
  const location = useLocation();
  const deleteProduct = () => {
    setDeletingCategoryId(location.pathname.split('/')[3]);
  };
  return ( 
    <div className={styles['editProduct']}>
      <div className={styles['deleteBody']}>
        <div className={styles['editTitle']}>
          Do You want to delete this product
        </div>
        <div className={styles['buttons']}>
          <button
            onClick={() => navigate('/admin/category-list')}
            className={styles['cancelButton']}
          >
            Cancel
          </button>
          <button className={styles['deleteButton']} onClick={deleteProduct}>
            Delete
          </button>
        </div>
      </div>
    </div>
  );
}

export default DeleteProductCategory;
