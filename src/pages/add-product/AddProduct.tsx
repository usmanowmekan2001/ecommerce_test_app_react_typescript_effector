import React, { useEffect } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import styles from './AddProduct.module.css';
import Dropzone from 'react-dropzone';
import { Select } from 'antd';
import { IColorOpt, IImgItem } from './types';
import CustomInput from '../../shared/ui/customInput/CustomInput';
import {
  $brands,
  $categories,
  $colors,
  $images,
  $productBrand,
  $productCategory,
  $productColor,
  $productDescription,
  $productPrice,
  $productQuantity,
  $productTags,
  $productTitle,
  addProductFormSubmitted,
  addProductPageMounted,
  changeBrand,
  changeCategory,
  changeColor,
  changeDescription,
  changePrice,
  changeQuantity,
  changeTags,
  changeTitle,
  uploadingImg,
} from './model/model';
import { useUnit } from 'effector-react';
import { setSpinner } from '../../shared/lib/spinner/model';

const AddProduct = () => {
  useEffect(() => {
    addProductPageMounted();
    // formik.values.color = color;
  }, []);
  const [
    productTitle,
    productPrice,
    productBrand,
    brands,
    productCategory,
    categories,
    productTags,
    productColor,
    colors,
    productQuantity,
    images,
    productDescription,
  ] = useUnit([
    $productTitle,
    $productPrice,
    $productBrand,
    $brands,
    $productCategory,
    $categories,
    $productTags,
    $productColor,
    $colors,
    $productQuantity,
    $images,
    $productDescription,
  ]);

  const colorOpt: IColorOpt[] = [];
  colors.forEach((i) => {
    colorOpt.push({
      label: i.title,
      value: i._id,
    });
  });
  const img: IImgItem[] = [];
  images.forEach((i) => {
    img.push({
      URL: i.URL,
      name: i.name,
    });
  });

  function handleCreate(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    setSpinner(true);
    addProductFormSubmitted();
  }

  return (
    <div>
      <h3 className={styles.title}>Add Product</h3>
      <div>
        <form
          action=""
          className={styles.addProductForm}
          onSubmit={handleCreate}
        >
          <div className="form-floating mt-3">
            <CustomInput
              className="py-3 mb-3"
              type="text"
              label="Enter Product Title"
              name="title"
              value={productTitle}
              onChange={(e) => changeTitle(e.target.value)}
              disabled={false}
            />
          </div>

          <div className={styles.productDescription}>
            <ReactQuill
              theme="snow"
              value={productDescription}
              onChange={(e) => changeDescription(e)}
            />
          </div>

          <CustomInput
            className=" py-3 mb-3"
            type="number"
            label="Enter Product Price"
            name="price"
            value={productPrice}
            onChange={(e) => changePrice(Number(e.target.value))}
            disabled={false}
          />

          <select
            name="brand"
            value={productBrand}
            onChange={(e) => changeBrand(e.target.value)}
            className="form-control py-3 mb-3"
            id=""
            disabled={false}
          >
            <option value="">Select Brand</option>
            {brands.map((i, j) => {
              return (
                <option key={j} value={i.title}>
                  {i.title}
                </option>
              );
            })}
          </select>
          <select
            name="category"
            value={productCategory}
            onChange={(e) => changeCategory(e.target.value)}
            onBlur={(e) => changeCategory(e.target.value)}
            className="form-control py-3 mb-3"
            id=""
            disabled={false}
          >
            <option value="">Select Category</option>
            {categories.map((i, j) => {
              return (
                <option key={j} value={i.title}>
                  {i.title}
                </option>
              );
            })}
          </select>
          <select
            name="tags"
            value={productTags}
            onChange={(e) => changeTags(e.target.value)}
            onBlur={(e) => changeTags(e.target.value)}
            className="form-control py-3 mb-3"
            id=""
            disabled={false}
          >
            <option value="" disabled>
              Select Category
            </option>
            <option value="featured">Featured</option>
            <option value="popular">Popular</option>
            <option value="special">Special</option>
          </select>

          <Select
            mode="multiple"
            allowClear
            className="w-100 mb-3"
            placeholder="Select colors"
            defaultValue={productColor}
            onChange={(i) => changeColor(i)}
            options={colorOpt}
            disabled={false}
          />

          <CustomInput
            className="form-control py-3 mb-3"
            type="number"
            label="Enter Product Quantity"
            name="quantity"
            value={productQuantity}
            onChange={(e) => changeQuantity(Number(e.target.value))}
            disabled={false}
          />

          <div className="bg-white p-5 border-1 text-center">
            <Dropzone
              onDrop={(acceptedFiles) => {
                uploadingImg(acceptedFiles);
              }}
            >
              {({ getRootProps, getInputProps }) => (
                <section>
                  <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <p>
                      Drag 'n' drop some files here, or click to select files
                    </p>
                  </div>
                </section>
              )}
            </Dropzone>
          </div>
          <div className={styles.AddProductImages}>
            {images?.map((i, j) => {
              return (
                <div key={j} className={styles.AddProductImageBlock}>
                  <button
                    type="button"
                    onClick={() => {
                      //   dispatch( (i.name));
                    }}
                    className="btn-close position-absolute"
                    style={{ top: '5px', right: '5px' }}
                  ></button>
                  <img
                    src={i.URL}
                    alt=""
                    className={styles.AddProductImage}
                    width={200}
                    height={200}
                  />
                </div>
              );
            })}
          </div>
          <button
            type="submit"
            className="btn btn-success border-0 rounded-3 my-5"
          >
            Add Product
          </button>
        </form>
      </div>
    </div>
  );
};

export default AddProduct;
