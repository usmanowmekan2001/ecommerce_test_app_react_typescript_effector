export interface IColorOpt {
  label: string;
  value: string;
}
export interface IBrandItem {
  createdAt: string;
  updatedAt: string;
  title: string;
  _id: string;
  __v: number;
}
export interface ICategoryItem extends IBrandItem {}
export interface IColorItem extends IBrandItem {}

export interface IImgItem {
  URL: string;
  name: string;
}
