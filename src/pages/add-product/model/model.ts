import {
  attach,
  createEffect,
  createEvent,
  createStore,
  sample,
} from 'effector';
import * as brands from '../../../shared/api/brand';
import * as color from '../../../shared/api/color';
import * as category from '../../../shared/api/category';
import * as upload from '../../../shared/api/upload';
import { IBrandItem, ICategoryItem, IColorItem, IImgItem } from '../types';

const getBrandsFx = attach({ effect: brands.getBrandsFx });
const uploadImgFx = attach({ effect: upload.uploadImgFx });
const getColorsFx = attach({ effect: color.getColorsFx });
const getCategoriesFx = attach({ effect: category.getCategoriesFx });

export const addProductFormSubmitted = createEvent();
export const Colors = createEvent<IColorItem[]>();
export const Categories = createEvent<ICategoryItem[]>();
export const Brands = createEvent<IBrandItem[]>();

export const changeTitle = createEvent<string>();
export const changeBrand = createEvent<string>();
export const changeTags = createEvent<string>();
export const changeDescription = createEvent<string>();
export const changePrice = createEvent<number>();
export const changeCategory = createEvent<string>();
export const changeQuantity = createEvent<number>();
export const changeColor = createEvent<string>();
export const addProductPageMounted = createEvent();
export const uploadingImg = createEvent<File[]>();
export const changeImages = createEvent<IImgItem[]>();

export const $images = createStore<IImgItem[]>([]).on(
  changeImages,
  (_, value) => value
);
export const $productTitle = createStore<string>('').on(
  changeTitle,
  (_, value) => value
);
export const $productDescription = createStore<string>('').on(
  changeDescription,
  (_, value) => value
);
export const $productPrice = createStore<number>(0).on(
  changePrice,
  (_, value) => value
);
export const $productQuantity = createStore<number>(0).on(
  changeQuantity,
  (_, value) => value
);
export const $productBrand = createStore<string>('').on(
  changeBrand,
  (_, value) => value
);
export const $productCategory = createStore<string>('').on(
  changeCategory,
  (_, value) => value
);
export const $productTags = createStore<string>('').on(
  changeCategory,
  (_, value) => value
);
export const $productColor = createStore<string | null>(null).on(
  changeColor,
  (_, value) => value
);
export const $upload = createStore<File[]>([]).on(
  uploadingImg,
  (_, value) => value
);
export const $colors = createStore<IBrandItem[]>([]).on(
  Colors,
  (_, value) => value
);
export const $brands = createStore<IBrandItem[]>([]).on(
  Brands,
  (_, value) => value
);
export const $categories = createStore<ICategoryItem[]>([]).on(
  Categories,
  (_, value) => value
);
const getInitialDates = createEffect(() => {
  getBrandsFx().then((brands) => Brands(brands));
  getColorsFx().then((colors) => Colors(colors));
  getCategoriesFx().then((categories) => Categories(categories));
});
const uploadingImages = createEffect((data: File[]) => {
  uploadImgFx(data).then((img) => changeImages(img));
});
sample({
  clock: addProductPageMounted,
  target: getInitialDates,
});
sample({
  clock: uploadingImg,
  source: $upload,
  target: uploadingImages,
});
