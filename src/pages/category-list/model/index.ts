import { attach, createEvent, createStore } from 'effector';
import { ICategoryData, ICategoryItem } from '../types';
import * as api from '../../../shared/api/category';

const getCategoriesFx = attach({ effect: api.getCategoriesFx });

export const setCategories = createEvent<ICategoryData[]>();
const fetchCategories = createEvent<boolean>();
export const $categoryLoading = createStore<boolean>(true).on(
  fetchCategories,
  (_, value) => value
);
export const $productCategories = createStore<ICategoryData[]>([]).on(
  setCategories,
  (_, value) => value
);

export const getCategories = async () => {
  const allCategories: ICategoryItem[] = await getCategoriesFx();
  const tableData: ICategoryData[] = [];

  for (let i = 0; i < allCategories.length; i++) {
    tableData.push({
      key: i + 1,
      id: allCategories[i]._id,
      title: allCategories[i].title,
    });
  }
  setCategories(tableData);
};
