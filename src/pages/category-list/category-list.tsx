import { useEffect, useState } from 'react';
import CheckAuth from '../../features/check-auth';
import styles from './category-list.module.css';
import { BiEdit } from 'react-icons/bi';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import { MdDelete } from 'react-icons/md';
import { useUnit } from 'effector-react';
import { Link } from 'react-router-dom';
import { $productCategories, getCategories } from './model';
import { setDeletingCategoryId } from '../delete-product-category/model';

/* eslint-disable-next-line */
export interface CategoryListProps {}

export function CategoryList(props: CategoryListProps) {
  useEffect(() => {
    getCategories();
  }, []);
  const [activeNumber, setActiveNumber] = useState(1);
  const pageNumbersData: string[] = [];
  const [categories] = useUnit([$productCategories]);

  const countPageNumbers = Math.ceil(categories.length / 5);
  for (let i = 1; i <= countPageNumbers; i++) {
    pageNumbersData.push(String(i));
  }
  const handleBack = () => {
    setActiveNumber((prev) => (prev !== 1 ? prev - 1 : 1));
  };
  const handleNext = () => {
    setActiveNumber((prev) =>
      prev === countPageNumbers ? countPageNumbers : prev + 1
    );
  };
  return (
    <CheckAuth>
      <div className={styles['container']}>
        <div className={styles.recentRow}>
          <div className={styles.recentRowTable}>
            <h3 className={styles.title}>Categories</h3>
            <table>
              <thead>
                <tr>
                  <th className={styles.sortTableTitle}>No.</th>
                  <th className={styles.sortTableTitle}>
                    <span>Title</span>
                  </th>
                  <th className={styles.tableTitle}>Action</th>
                </tr>
              </thead>
              <tbody>
                {categories
                  .slice((activeNumber - 1) * 5, (activeNumber - 1) * 5 + 5)
                  .map((el, index) => {
                    return (
                      <tr key={index}>
                        <td>{el.key}</td>
                        <td>{el.title}</td>
                        <td>
                          {' '}
                          <div className="d-flex">
                            <Link
                              to={`/admin/product-category-update/${el.id}`}
                              className=" fs-3 text-danger"
                            >
                              <BiEdit />
                            </Link>
                            <Link
                              to={'/admin/product-category-delete'}
                              className="ms-3 fs-3 text-danger d-flex"
                            >
                              <MdDelete
                                onClick={() => {
                                  setDeletingCategoryId(el.id);
                                }}
                              />
                            </Link>
                          </div>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
        <div className={styles.page}>
          <div className={styles.pageIcon} onClick={handleBack}>
            <IoIosArrowBack />
          </div>
          <div className={styles.pageNumbers}>
            {pageNumbersData.map((number, index) => {
              return (
                <span
                  id={String(index + 1)}
                  key={index}
                  className={
                    activeNumber === Number(number)
                      ? `${styles.pageNumber} ${styles.activeNumber}`
                      : styles.pageNumber
                  }
                  onClick={(e) => {
                    setActiveNumber(Number(e.currentTarget.id));
                  }}
                >
                  {number}
                </span>
              );
            })}
          </div>
          <div className={styles.pageIcon} onClick={handleNext}>
            <IoIosArrowForward />
          </div>
        </div>
      </div>
    </CheckAuth>
  );
}

export default CategoryList;
