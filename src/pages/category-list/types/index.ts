export interface ICategoryItem {
  createdAt: string;
  title: string;
  updatedAt: string;
  __v: number;
  _id: string;
}
export interface ICategoryData {
  key: number;
  id: string;
  title: string;
}
