export interface IColorOpt {
  label: string;
  value: string;
}
export interface IProductData {
  key: number;
  id: string;
  title: string;
  price: number;
  quantity: number;
  brand: string;
  category: string;
  color: string[];
}
export interface IProductItem {
  brand: string;
  category: string;
  color: string[];
  createdAt: string;
  updatedAt: string;
  description: string;
  images: object[];
  ratings: object[];
  price: number;
  quantity: number;
  sold: number;
  slug: string;
  tags: string;
  title: string;
  totalrating: string;
  _id: string;
}
