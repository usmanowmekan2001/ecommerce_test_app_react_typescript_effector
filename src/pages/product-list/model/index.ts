import { attach, createEvent, createStore } from 'effector';
import { IAlert, setAlert } from '../../../shared/lib/alert/model/model';
import { IProductData, IProductItem } from '../types';
import * as api from '../../../shared/api/product';

export const setProducts = createEvent<IProductData[]>();
const fetchProducts = createEvent<boolean>();
export const $productsLoading = createStore<boolean>(true).on(
  fetchProducts,
  (_, value) => value
);
export const $products = createStore<IProductData[]>([]).on(
  setProducts,
  (_, value) => value
);
const getProductsFx = attach({ effect: api.getProductsFx });

export const handleAlertMessage = (alert: IAlert) => {
  setAlert(alert);
  setTimeout(() => setAlert({ alertText: '', alertStatus: '' }), 3000);
};

export const getProducts = async () => {
  fetchProducts(true);
  const allProducts: IProductItem[] = await getProductsFx();
  const tableData: IProductData[] = [];

  for (let i = 0; i < allProducts.length; i++) {
    tableData.push({
      key: i + 1,
      id: allProducts[i]._id,
      title: allProducts[i].title,
      brand: allProducts[i].brand,
      category: allProducts[i].category,
      quantity: allProducts[i].quantity,
      color: allProducts[i].color,
      price: allProducts[i].price,
    });
  }
  setProducts(tableData);
  fetchProducts(false);
};
