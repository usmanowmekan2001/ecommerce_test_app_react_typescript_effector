import { useEffect, useState } from 'react';
import CheckAuth from '../../features/check-auth';
import { BiEdit } from 'react-icons/bi';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import { MdDelete } from 'react-icons/md';
import styles from './product-list.module.css';

import { $products, $productsLoading, getProducts } from './model';
import { useGate, useUnit } from 'effector-react';
import { Link } from 'react-router-dom';
import { $deletingId } from '../delete-product/model';
import { ProductPageGate } from '../../shared/router';

/* eslint-disable-next-line */
export interface ProductListProps {}

export function ProductList(props: ProductListProps) {
  useGate(ProductPageGate);
  const [productState, deletingId, productsLoading] = useUnit([
    $products,
    $deletingId,
    $productsLoading,
  ]);
  useEffect(() => {
    getProducts();
  }, [deletingId]);
  const [activeNumber, setActiveNumber] = useState(1);
  const pageNumbersData: string[] = [];

  const countPageNumbers = Math.ceil(productState.length / 5);
  for (let i = 1; i <= countPageNumbers; i++) {
    pageNumbersData.push(String(i));
  }
  const handleBack = () => {
    setActiveNumber((prev) => (prev !== 1 ? prev - 1 : 1));
  };
  const handleNext = () => {
    setActiveNumber((prev) =>
      prev === countPageNumbers ? countPageNumbers : prev + 1
    );
  };
  return (
    <CheckAuth>
      <div className={styles['container']}>
        <div className={styles.recentRow}>
          <div className={styles.recentRowTable}>
            <h3 className={styles.title}>Products</h3>
            {!productsLoading ? (
              <table>
                <thead>
                  <tr>
                    <th className={styles.sortTableTitle}>No.</th>
                    <th className={styles.sortTableTitle}>
                      <span>Title</span>
                    </th>
                    <th className={styles.sortTableTitle}>
                      <span>Brand</span>
                    </th>
                    <th className={styles.sortTableTitle}>
                      <span>Category</span>
                    </th>
                    <th className={styles.sortTableTitle}>
                      <span>Quantity</span>
                    </th>
                    <th className={styles.sortTableTitle}>
                      <span>Color</span>
                    </th>
                    <th className={styles.sortTableTitle}>
                      <span>Price</span>
                    </th>
                    <th className={styles.tableTitle}>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {productState
                    .slice((activeNumber - 1) * 5, (activeNumber - 1) * 5 + 5)
                    .map((el, index) => {
                      return (
                        <tr key={index}>
                          <td>{el.key}</td>
                          <td>{el.title}</td>
                          <td>{el.brand}</td>
                          <td>{el.category}</td>
                          <td>{el.quantity}</td>
                          <td>
                            {el.color?.map((i, j) => {
                              return <p key={j}>{i}</p>;
                            })}
                          </td>
                          <td>{el.price}</td>
                          <td>
                            <div className="d-flex">
                              <Link
                                to={`/admin/product-update/${el.id}`}
                                className=" fs-3 text-danger"
                              >
                                <BiEdit />
                              </Link>
                              <Link
                                to={`/admin/product-delete/${el.id}`}
                                className="ms-3 fs-3 text-danger d-flex"
                              >
                                <MdDelete />
                              </Link>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
            ) : (
              <p>Loading...</p>
            )}
          </div>
        </div>
        <div className={styles.page}>
          <div className={styles.pageIcon} onClick={handleBack}>
            <IoIosArrowBack />
          </div>
          <div className={styles.pageNumbers}>
            {pageNumbersData.map((number, index) => {
              return (
                <span
                  id={String(index + 1)}
                  key={index}
                  className={
                    activeNumber === Number(number)
                      ? `${styles.pageNumber} ${styles.activeNumber}`
                      : styles.pageNumber
                  }
                  onClick={(e) => {
                    setActiveNumber(Number(e.currentTarget.id));
                  }}
                >
                  {number}
                </span>
              );
            })}
          </div>
          <div className={styles.pageIcon} onClick={handleNext}>
            <IoIosArrowForward />
          </div>
        </div>
      </div>
    </CheckAuth>
  );
}
export default ProductList;
