import { Outlet, useLocation } from 'react-router-dom';
import styles from './main-layout.module.css';
import { useEffect, useState } from 'react';

import Sidebar from './components/Sidebar/Sidebar';
import Header from './components/Header/Header';
/* eslint-disable-next-line */
export interface MainLayoutProps {}

export function MainLayout(props: MainLayoutProps) {
  const location = useLocation();
  const currentLocation = location.pathname.split('/')[2];
  const [selected, setSelected] = useState({
    id: 'users',
    select: true,
  });
  
  useEffect(() => {
    if (currentLocation === undefined) {
      setSelected({ id: 'admin', select: true });
    } else {
      setSelected({ id: currentLocation, select: true });
    }
  }, []);

  const [smallSidebar, setSmallSidebar] = useState(false);
  return (
    <div className={styles['container']}>
      <div className={styles.mainLayoutWrapper}>
        <Sidebar
          selected={selected}
          setSelected={setSelected}
          smallSidebar={smallSidebar}
        />
        <div className={styles.layoutBody}>
          <Header
            smallSidebar={smallSidebar}
            setSmallSidebar={setSmallSidebar}
          />
          <div className={styles.main}>
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainLayout;
