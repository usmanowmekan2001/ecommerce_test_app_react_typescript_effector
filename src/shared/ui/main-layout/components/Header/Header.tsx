import React from 'react';
import { Link } from 'react-router-dom';
import styles from '../../main-layout.module.css';
import image from '../../images/main-banner-1.jpg';
import { AiOutlineMenuFold, AiOutlineMenuUnfold } from 'react-icons/ai';
import { IoIosNotifications } from 'react-icons/io';
const Header = ({
  setSmallSidebar,
  smallSidebar,
}: {
  setSmallSidebar: React.Dispatch<React.SetStateAction<boolean>>;
  smallSidebar: boolean;
}) => {
  return (
    <div className={styles.header}>
      <div
        className={styles.headerIcon}
        onClick={() => setSmallSidebar((prev) => !prev)}
      >
        {smallSidebar ? <AiOutlineMenuUnfold /> : <AiOutlineMenuFold />}
      </div>
      <div className={styles.headerRightElements}>
        <div className={styles.notification}>
          <IoIosNotifications />
          <span className={styles.notificationNumber}>5</span>
        </div>
        <div className="dropdown">
          <div className={styles.personalDates}>
            <div className={styles.headerImage}>
              <img src={image} alt="" />
            </div>
            <div
              role="button"
              id="dropdownMenuLink"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <h5 className={styles.name}>Mekan</h5>
              <p className={styles.account}>usmanowmekan2001@gmail.com</p>
            </div>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <li>
                <Link to="/" className="dropdown-item py-1 mb-1">
                  View Profile
                </Link>
              </li>
              <li>
                <Link to="/" className="dropdown-item py-1 mb-1">
                  Sign Out
                </Link>
              </li>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
