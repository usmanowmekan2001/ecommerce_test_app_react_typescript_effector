export interface ISelected {
  id: string;
  select: boolean;
}
