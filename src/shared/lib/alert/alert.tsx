import styles from './alert.module.css';

/* eslint-disable-next-line */

export interface IAlertProps {
  props: IAlert;
}
export interface IAlert {
  alertText: string;
  alertStatus: string;
}
export function Alert(props: IAlertProps) {
  return (
    <div className={styles['container']}>
      <div
        className={
          props.props.alertStatus === 'warning'
            ? styles['alert-warning']
            : styles['alert-success']
        }
      >
        {props.props.alertText}
      </div>
    </div>
  );
}

export default Alert;
