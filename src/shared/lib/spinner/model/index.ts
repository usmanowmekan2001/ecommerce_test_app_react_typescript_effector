import { createEvent, createStore } from 'effector';

export const setSpinner = createEvent<boolean>();

export const $spinner = createStore<boolean>(false).on(
  setSpinner,
  (_, value) => value
);
