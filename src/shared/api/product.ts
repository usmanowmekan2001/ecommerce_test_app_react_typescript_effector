import axios from 'axios';
import { createEffect } from 'effector';
import { setSpinner } from '../lib/spinner/model';
import { handleAlertMessage } from '../lib/alert/model/model';

export const base_url = 'http://localhost:5000/api/';

export interface IProductData {
  id: string;
  title: string;
  price: number;
  quantity: number;
  brand: string;
  category: string;
  color: string[];
}
export const getProductsFx = createEffect(async () => {
  try {
    const response = await axios.get(`${base_url}product/`);
    console.log(response);

    if (response.data) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
});
export const updateProductFx = createEffect(
  async (productData: IProductData) => {
    const getTokenFromLocalStorage = localStorage.getItem('token')
      ? JSON.parse(localStorage.getItem('token') as string)
      : null;
    const config = {
      headers: {
        Authorization: `Bearer ${
          getTokenFromLocalStorage !== null ? getTokenFromLocalStorage : ''
        }`,
        Accept: 'application/json',
      },
    };

    try {
      const response = await axios.put(
        `${base_url}product/${productData.id}`,
        {
          title: productData.title,
          price: productData.price,
          quantity: productData.quantity,
          color: productData.color,
          brand: productData.brand,
          category: productData.category,
        },
        config
      );

      if (response.data) {
        handleAlertMessage({
          alertText: 'Product updated successfully',
          alertStatus: 'success',
        });
        setSpinner(false);
      }
    } catch (error) {
      console.log(error);
      handleAlertMessage({
        alertText: 'Something went wrong',
        alertStatus: 'warning',
      });
      setSpinner(false);
    }
  }
);

export const getSingleProductFx = createEffect(async (id: string) => {
  const response = await axios.get(`${base_url}product/${id}`);
  if (response.data) {
    return response.data;
  }
});

export const deleteProductFx = createEffect(async (id: string) => {
  const getTokenFromLocalStorage = localStorage.getItem('token')
    ? JSON.parse(localStorage.getItem('token') as string)
    : null;
  const config = {
    headers: {
      Authorization: `Bearer ${
        getTokenFromLocalStorage !== null ? getTokenFromLocalStorage : ''
      }`,
      Accept: 'application/json',
    },
  };
  try {
    const response = await axios.delete(`${base_url}product/${id}`, config);
    console.log(response);

    if (response.data) {
      handleAlertMessage({
        alertText: 'Product deleted successfully',
        alertStatus: 'success',
      });
    }
  } catch (error) {
    console.log(error);
    handleAlertMessage({
      alertText: 'Something went wrong',
      alertStatus: 'warning',
    });
  }
});
