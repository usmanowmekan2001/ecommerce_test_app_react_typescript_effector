import axios from "axios";
import { createEffect } from "effector";


export interface IBrandItem {
  createdAt: string;
  updatedAt: string;
  title: string;
  _id: string;
  __v: number;
}
export const base_url = 'http://localhost:5000/api/';

export const getBrandsFx = createEffect<void, IBrandItem[]>(async () => {
  try {
    const response = await axios.get(`${base_url}brand/`);
    console.log(response);

    if (response.data) {
      const data = response.data;
      return data;
    }
  } catch (error) {
    console.log(error);
  }
});
