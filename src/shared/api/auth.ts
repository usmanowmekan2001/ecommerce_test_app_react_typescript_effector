import axios from 'axios';
import { createEffect } from 'effector';
import { handleAlertMessage } from '../lib/alert/model/model';

export interface IUser {
  email: string;
  password: string;
}
export const base_url = 'http://localhost:5000/api/';
type User = {
  email: string;
  firstname: string;
  lastname: string;
  mobile: string;
  token: string;
  _id: string;
};
type SignInError =
  | { error: 'Invalid Credentials' }
  | { error: 'invalid_request' };
export const loginFx = createEffect<IUser, User, SignInError>(async (data) => {
  return axios
    .post(`${base_url}user/login`, data)
    .then((response) => {
      handleAlertMessage({
        alertText: 'You logged successfully',
        alertStatus: 'success',
      });
      return response.data;
    })
    .catch((error) => {
      console.log(Promise.reject(error.response.data));

      handleAlertMessage({
        alertText: 'Something went wrong',
        alertStatus: 'warning',
      });
      return Promise.reject(error.response.data);
    });
});

export const getUsersFx = createEffect(async () => {
  try {
    const response = await axios.get(`${base_url}user/all-users`);
    if (response.data) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
});
export const getOrdersFx = createEffect(async () => {
  const getTokenFromLocalStorage = localStorage.getItem('token')
    ? JSON.parse(localStorage.getItem('token') as string)
    : null;
  const config = {
    headers: {
      Authorization: `Bearer ${
        getTokenFromLocalStorage !== null ? getTokenFromLocalStorage : ''
      }`,
      Accept: 'application/json',
    },
  };
  try {
    const response = await axios.get(`${base_url}user/get-all-orders`, config);
    if (response.data) {
      return response.data.orders;
    }
  } catch (error) {
    console.log(error);
  }
});
export const getMonthlyOrderFx = createEffect(async () => {
  const getTokenFromLocalStorage = localStorage.getItem('token')
    ? JSON.parse(localStorage.getItem('token') as string)
    : null;
  const config = {
    headers: {
      Authorization: `Bearer ${
        getTokenFromLocalStorage !== null ? getTokenFromLocalStorage : ''
      }`,
      Accept: 'application/json',
    },
  };
  try {
    const response = await axios.get(
      `${base_url}user/getMonthWiseOrderIncome`,
      config
    );
    if (response.data) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
});
export const getYearlyStatsFx = createEffect(async () => {
  const getTokenFromLocalStorage = localStorage.getItem('token')
    ? JSON.parse(localStorage.getItem('token') as string)
    : null;
  const config = {
    headers: {
      Authorization: `Bearer ${
        getTokenFromLocalStorage !== null ? getTokenFromLocalStorage : ''
      }`,
      Accept: 'application/json',
    },
  };
  try {
    const response = await axios.get(`${base_url}user/getYearlyOrders`, config);
    if (response.data) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
});
