import axios from 'axios';
import { createEffect } from 'effector';
export const base_url = 'http://localhost:5000/api/';

export const getColorsFx = createEffect(async () => {
  try {
    const response = await axios.get(`${base_url}color/`);
    console.log(response);

    if (response.data) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
});
