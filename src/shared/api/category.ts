import axios from 'axios';
import { createEffect } from 'effector';
import { handleAlertMessage } from '../lib/alert/model/model';
import { setSpinner } from '../lib/spinner/model';
export const base_url = 'http://localhost:5000/api/';
export interface IProductCategoryData {
  id: string;
  title: string;
}
export const getCategoriesFx = createEffect(async () => {
  try {
    const response = await axios.get(`${base_url}category/`);

    if (response.data) {
      return response.data;
    }
  } catch (error) {
    console.log(error);
  }
});

export const getProductCategoryFx = createEffect(async (id: string) => {
  const getTokenFromLocalStorage = localStorage.getItem('token')
    ? JSON.parse(localStorage.getItem('token') as string)
    : null;
  const config = {
    headers: {
      Authorization: `Bearer ${
        getTokenFromLocalStorage !== null ? getTokenFromLocalStorage : ''
      }`,
      Accept: 'application/json',
    },
  };
  try {
    const response = await axios.get(`${base_url}category/${id}`, config);
    return response.data.title;
  } catch (error) {
    console.log(error);
  }
});

export const updateProductCategoryFx = createEffect(
  async (productCategoryData: IProductCategoryData) => {
    const getTokenFromLocalStorage = localStorage.getItem('token')
      ? JSON.parse(localStorage.getItem('token') as string)
      : null;
    const config = {
      headers: {
        Authorization: `Bearer ${
          getTokenFromLocalStorage !== null ? getTokenFromLocalStorage : ''
        }`,
        Accept: 'application/json',
      },
    };
    if (!productCategoryData.title) {
      setSpinner(false);
      handleAlertMessage({
        alertText: 'Заполните все поля',
        alertStatus: 'warning',
      });
      return;
    }
    try {
      const response = await axios.put(
        `${base_url}category/${productCategoryData.id}`,
        {
          title: productCategoryData.title,
        },
        config
      );
      console.log(response);

      if (response.data) {
        handleAlertMessage({
          alertText: 'Category updated successfully',
          alertStatus: 'success',
        });
        setSpinner(false);
        return true;
      }
    } catch (error) {
      console.log(error);
      handleAlertMessage({
        alertText: 'Something went wrong',
        alertStatus: 'warning',
      });
      setSpinner(false);
      return false;
    }
  }
);

export const deleteProductCategoryFx = createEffect(async (id: string) => {
  const getTokenFromLocalStorage = localStorage.getItem('token')
    ? JSON.parse(localStorage.getItem('token') as string)
    : null;
  const config = {
    headers: {
      Authorization: `Bearer ${
        getTokenFromLocalStorage !== null ? getTokenFromLocalStorage : ''
      }`,
      Accept: 'application/json',
    },
  };
  try {
    const response = await axios.delete(`${base_url}category/${id}`, config);
    console.log(response);

    if (response.data) {
      handleAlertMessage({
        alertText: 'Product deleted successfully',
        alertStatus: 'success',
      });
    }
  } catch (error) {
    console.log(error);
    handleAlertMessage({
      alertText: 'Something went wrong',
      alertStatus: 'warning',
    });
  }
});
