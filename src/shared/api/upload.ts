import axios from 'axios';
import { createEffect } from 'effector';
export const base_url = 'http://localhost:5000/api/';
export interface IImgItem {
  URL: string;
  name: string;
}
export const uploadImgFx = createEffect<File[], IImgItem[], Error>(
  async (data) => {
    const getTokenFromLocalStorage = localStorage.getItem('token')
      ? JSON.parse(localStorage.getItem('token') as string)
      : null;
    const config = {
      headers: {
        Authorization: `Bearer ${
          getTokenFromLocalStorage !== null ? getTokenFromLocalStorage : ''
        }`,
        Accept: 'application/json',
      },
    };
    try {
      const formData = new FormData();
      for (let i = 0; i < data.length; i++) {
        formData.append('images', data[i]);
      }
      const response = await axios.post(`${base_url}upload`, formData, config);

      if (response.data) {
        return response.data;
      }
    } catch (error) {
      console.log(error);
    }
  }
);
