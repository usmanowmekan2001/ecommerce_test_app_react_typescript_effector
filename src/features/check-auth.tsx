import { Navigate } from 'react-router-dom';
import { $auth, setAuth } from '../pages/login/model/model';
import { useUnit } from 'effector-react';
import React, { ReactNode } from 'react';

/* eslint-disable-next-line */
export interface CheckAuthProps {
  children: ReactNode;
}

function CheckAuth({ children }: CheckAuthProps) {
  const [auth] = useUnit([$auth]);
  const removeUser = () => {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    setAuth(false);
  };
  try {
    const lSData = JSON.parse(localStorage.getItem('token') as string);
    const userData = JSON.parse(localStorage.getItem('user') as string);

    if (!lSData || !userData) {
      removeUser();
    }
  } catch (error) {
    removeUser();
  }

  return auth ? children : <Navigate to="/" replace={true} />;
}
export function BlockLogin({ children }: CheckAuthProps) {
  const [auth] = useUnit([$auth]);
  const removeUser = () => {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    setAuth(false);
  };
  try {
    const lSData = JSON.parse(localStorage.getItem('token') as string);
    const userData = JSON.parse(localStorage.getItem('user') as string);

    if (!lSData || !userData) {
      removeUser();
    }
  } catch (error) {
    removeUser();
  }

  return auth ? <Navigate to="/admin" replace={true} /> : children;
}

export default CheckAuth;
