// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import { useUnit } from 'effector-react';
import Alert from '../shared/lib/alert/alert';
import { $alert } from '../shared/lib/alert/model/model';
import { $router } from '../shared/router';

export function App() {
  const [alert, routing] = useUnit([$alert, $router]);
  if (routing !== null) {
    return (
      <>
        {alert.alertText && <Alert props={alert} />}
        <RouterProvider
          router={routing}
          fallbackElement={<p>Initial Load...</p>}
        />
      </>
    );
  }
}

export default App;
