import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';

import App from './app';
// import { BrowserRouter } from 'react-router-dom';
import { setAuth } from '../pages/login/model/model';
import Login from '../pages/login/login';
import MainLayout from '../shared/ui/main-layout/main-layout';
import Users from '../pages/users/users';
import ProductList from '../pages/product-list/product-list';
import CategoryList from '../pages/category-list/category-list';
import DeleteProduct from '../pages/delete-product/delete-product';
import UpdateProduct from '../pages/update-product/update-product';
import UpdateProductCategory from '../pages/update-product-category/update-product-category';
import DeleteProductCategory from '../pages/delete-product-category/delete-product-category';
import { createBrowserRouter } from 'react-router-dom';
import { changeRouter } from '../shared/router';
import Dashboard from '../pages/dashboard/DashBoard';
import AddProduct from '../pages/add-product/AddProduct';
const checkAuth = () => {
  const lSData = JSON.parse(localStorage.getItem('token') as string);
  const userData = JSON.parse(localStorage.getItem('user') as string);

  if (!lSData || !userData) {
    localStorage.clear();
    setAuth(false);
  } else {
    setAuth(true);
  }
};
const router = createBrowserRouter([
  {
    path: '/',
    element: <Login />,
  },
  {
    path: '/admin',
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <Dashboard />,
      },
      {
        path: 'customers',
        element: <Users />,
      },
      {
        path: 'product',
        element: <AddProduct />,
      },
      {
        path: 'product-list',
        element: <ProductList />,
      },
      {
        path: 'category-list',
        element: <CategoryList />,
      },
      {
        path: 'product-delete',
        element: <DeleteProduct />,
      },
      {
        path: 'product-update/:id',
        element: <UpdateProduct />,
      },
      {
        path: 'product-category-update/:id',
        element: <UpdateProductCategory />,
      },
      {
        path: 'product-category-delete',
        element: <DeleteProductCategory />,
      },
    ],
  },
]);
const renderApp = async () => {
  checkAuth();
  changeRouter(router);

  const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
  );
  root.render(
    <StrictMode>
      <App />
      {/* <BrowserRouter>
      </BrowserRouter> */}
    </StrictMode>
  );
};
renderApp();
